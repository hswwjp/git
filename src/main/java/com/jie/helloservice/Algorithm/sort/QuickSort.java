package com.jie.helloservice.Algorithm.sort;

import java.util.Arrays;

/**
 * 快速排序 - 填坑法
 */
public class QuickSort {

    /**
     * 循环调用快速排序自身
     * @param arr
     * @param startIndex
     * @param endIndex
     */
    public static void quickSort(int[] arr, int startIndex, int endIndex) {
        //递归结束条件:startIndex 大于等于endIndex 的时候
        if (startIndex >= endIndex) {
            return;
        }
        //得到基准元素位置
        int pivotIndex = partition(arr, startIndex, endIndex);
        //用分治法递归数列的两部分
        quickSort(arr, startIndex, pivotIndex - 1);
        quickSort(arr, pivotIndex + 1, endIndex);
    }

    /**
     * 得到基准元素位置,并对基准元素两边进行从大到小的排序
     * @param arr
     * @param startIndex
     * @param endIndex
     * @return
     */
    private static int partition(int[] arr, int startIndex, int endIndex) {
        //取第一个位置的元素作为基准元素
        int pivot = arr[startIndex];
        int left = startIndex;
        int right = endIndex;
        //坑的位置,初始等于 pivot 的位置
        int index = startIndex;

        //大循环在左右指针重合或者交错时结束
        while (right >= left) {
            //right 指针从右向左进行比较
            while (right >= left) {
                //如果右边的值比基准值小则交换
                if (arr[right] < pivot) {
                    //左边的值设成小的值
                    arr[left] = arr[right];
                    //坑的位置放到右边
                    index = right;
                    //左边指针需向右移
                    left++;
                    //右边比较完成,跳出循环
                    break;
                }
                right--;
            }
            //left 指针从左向右进行比较
            while (right >= left) {
                if (arr[left] > pivot) {
                    arr[right] = arr[left];
                    index = left;
                    right--;
                    break;
                }
                left++;
            }
        }
        //最后坑的位置设成最初的基准值
        arr[index] = pivot;
        return index;
    }

    public static void main(String[] args) {
        int[] arr = new int[]{4, 7, 6, 5, 3, 2, 8, 1};
        long l = System.nanoTime();
        quickSort(arr, 0, arr.length - 1);
        System.out.println(System.nanoTime() - l);
        System.out.println(Arrays.toString(arr));
    }
}
