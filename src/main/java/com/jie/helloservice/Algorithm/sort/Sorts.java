package com.jie.helloservice.Algorithm.sort;

import java.util.Arrays;

/**
 * 冒泡排序、插入排序、选择排序
 *
 * Author: Zheng
 */
public class Sorts {

    // 冒泡排序，a是数组，n表示数组大小
    public static void bubbleSort(int[] a, int n) {
        if (n <= 1) return;

        for (int i = 0; i < n; ++i) {
            // 提前退出标志位
            boolean flag = false;
            for (int j = 0; j < n - i - 1; ++j) {
                if (a[j] > a[j+1]) { // 交换
                    int tmp = a[j];
                    a[j] = a[j+1];
                    a[j+1] = tmp;
                    // 此次冒泡有数据交换
                    flag = true;
                }
            }
            if (!flag) break;  // 没有数据交换，提前退出
        }
    }

    // 插入排序，a表示数组，n表示数组大小
    public static void insertionSort(int[] a, int n) {
        if (n <= 1) return;

        // 双层循环，两个指针一前一后
        for (int i = 1; i < n; ++i) {
            int value = a[i]; // 记录前指针的值
            int j = i - 1; // 每次后指针都设为前指针的后一位

            // 查找要插入的位置并移动数据
            // 遇到值比前指针的值小，跳出循环，此时的后指针所指的值比前指针小
            for (; j >= 0; --j) {
                if (a[j] > value) {
                    a[j+1] = a[j];
                } else {
                    break;
                }
            }

            // 此时，后指针所指的值比 value(前指针所指的值)，可以在该下标后面插入 value(前指针所指的值)，并且这时候的数据已经全部往后移了
            a[j+1] = value;
        }
    }

    // 选择排序，a表示数组，n表示数组大小
    public static void selectionSort(int[] a, int n) {
        if (n <= 1) return;
        for (int i = 0; i < n - 1; ++i) {
            // 查找最小值
            int minIndex = i;
            for (int j = i + 1; j < n; ++j) {
                if (a[j] < a[minIndex]) {
                    minIndex = j;
                }
            }

            // 交换
            int tmp = a[i];
            a[i] = a[minIndex];
            a[minIndex] = tmp;
        }
    }

    public static void main(String[] args) {
        int[] arr = new int[]{4, 7, 9, 5, 1, 3, 2, 6};
        long start = System.nanoTime();
        bubbleSort(arr, arr.length);
        long end = System.nanoTime();
        System.out.println("冒泡排序耗时：" + (end - start) / Math.pow(10, 6) + "s");
        System.out.println(Arrays.toString(arr));

        arr = new int[]{4, 7, 9, 5, 1, 3, 2, 6};
        long start2 = System.nanoTime();
        insertionSort(arr, arr.length);
        long end2 = System.nanoTime();
        System.out.println("插入排序耗时：" + (end2 - start2) / Math.pow(10, 6) + "s");
        System.out.println(Arrays.toString(arr));

        arr = new int[]{4, 7, 9, 5, 1, 3, 2, 6};
        long start3 = System.nanoTime();
        selectionSort(arr, arr.length);
        long end3 = System.nanoTime();
        System.out.println("选择排序耗时：" + (end3 - start3) / Math.pow(10, 6) + "s");

        System.out.println(Arrays.toString(arr));
    }

}
