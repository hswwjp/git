package com.jie.helloservice.Algorithm.sort;

import java.util.Arrays;

/**
 * 快速排序
 *
 * Author: Zheng
 */
public class QuickSort4 {

    public static void main(String[] args) {
        int[] arr = new int[]{4, 7, 9, 5, 1, 3, 2, 6};
        long start = System.nanoTime();

        quickSort(arr, arr.length);

        long end = System.nanoTime();
        System.out.println("快速排序耗时：" + (end - start) / Math.pow(10, 6) + "s");
        System.out.println(Arrays.toString(arr));
    }

    // 快速排序，a是数组，n表示数组的大小
    public static void quickSort(int[] a, int n) {
        quickSortInternally(a, 0, n-1);
    }

    // 快速排序递归函数，p,r为下标
    private static void quickSortInternally(int[] a, int p, int r) {
        if (p >= r) return;

        int q = partition(a, p, r); // 获取分区点
        quickSortInternally(a, p, q-1);
        quickSortInternally(a, q+1, r);
    }

    private static int partition(int[] a, int p, int r) {
        int pivot = a[r];   // 设置数组最后一位为分区点
        int i = p; // 记录指针的移动

        // 第一次交换之前指针左边的数都是大于分区点的值，每次交换完之后总能保证把比分区点小的值调换到指针的左边来，因为调换完之后指针往前移动了一位
        // 遍历整个区间并做排序
        for(int j = p; j < r; ++j) {
            // 如果当前值小于分区点的值，则交换当前指针所指和当前遍历到的数，
            // 因为遇到大于分区点的值不做移动，所以这时会把小于分区点的值调换到左边
            // 交换之后将指针往前移动一位
            if (a[j] < pivot) {
                int tmp = a[i];
                a[i] = a[j];
                a[j] = tmp;
                ++i;
            }
        }

        // 最后交换分区点和指针所指的值，这样分区点左边都是小于的，右边都是大于的
        int tmp = a[i];
        a[i] = a[r];
        a[r] = tmp;

        System.out.println("i=" + i);
        return i;   // 返回当前指针下标，即为分区点
    }

}
