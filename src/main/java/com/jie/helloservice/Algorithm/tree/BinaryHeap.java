package com.jie.helloservice.Algorithm.tree;

import java.util.Arrays;

/**
 * 构建二叉堆
 */
public class BinaryHeap {

    /**
     * 构建堆
     * @param array     待调整的堆
     */
    public static void buildHeap(int[] array) {
        for (int i = array.length / 2; i >= 0; i--) {
            downAdjust(array, i, array.length - 1);
        }
    }

    /**
     * 上浮调整
     * @param array     待调整的堆
     */
    public static void upAdjust(int[] array) {
        int childIndex = array.length - 1;
        int parentIndex = (childIndex - 1) / 2;
        //temp 保存在插入的叶子节点值，用于最后的赋值
        int temp = array[childIndex];
        while (childIndex > 0 && temp < array[parentIndex]) {
            //无需真正交换，单向赋值即可
            array[childIndex] = array[parentIndex]; //如果进入循环,那么就可以调换两个节点的值了
            //为下一次循环做准备
            childIndex = parentIndex; //把子节点设置成原来的父节点
            parentIndex = (parentIndex - 1) / 2; //再把父节点设置成原来的父节点的父节点
        }
        //循环结束,说明这时找不到比最初的子节点值还小的父级节点,不再进行交换值
        array[childIndex] = temp;
    }

    /**
     * 下沉调整
     * @param array     待调整的堆
     * @param parentIndex       要下沉的父节点
     * @param length        堆的有效大小
     */
    public static void downAdjust(int[] array, int parentIndex, int length) {
        //temp 保存父节点值，用于最后的赋值
        int temp = array[parentIndex];
        int childIndex = 2 * parentIndex + 1;
        //如果子节点小于等于最后一个索引值,则进行循环
        while (childIndex < length) {
            //如果有右孩子,并且右孩子小于左孩子的值,则将索引定位到右孩子
            if (childIndex + 1 < length && array[childIndex + 1] < array[childIndex]) {
                childIndex ++;
            }
            //如果父节点小于子节点(这里的子节点已经是最小的那一个)的值，直接跳出
            if (temp <= array[childIndex])
                break;
            //无需真正交换，单向赋值即可
            array[parentIndex] = array[childIndex];//将父节点的值设置成子节点的值
            //为下一次循环做准备
            parentIndex = childIndex;
            childIndex = 2 * childIndex + 1;
        }
        array[parentIndex] = temp;
    }

    public static void main(String[] args) {
        int[] array = new int[]{1, 3, 2, 6, 5, 7, 8, 9, 10, 0};
        upAdjust(array);
        System.out.println(Arrays.toString(array));

        array = new int[]{7, 1, 3, 10, 5, 2, 8, 9, 6};
        buildHeap(array);
        System.out.println(Arrays.toString(array));
    }
}
