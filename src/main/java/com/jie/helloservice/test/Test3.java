package com.jie.helloservice.test;

import com.jie.helloservice.common.http.HttpUtils;
import com.jie.helloservice.common.http.RequestException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Scanner;

public class Test3 {

    private static String key = "https://apis.map.qq.com/ws/distance/v1/?mode=driving&key=PWWBZ-W6Q6P-B6PDQ-VVMJK-YS2BH-HTBI6";

//    public static void main(String[] args) throws RequestException {
//        String url = key + "&from=" + "39.983171,116.308479" + "&to=" + "39.996060,120.353455";
//        //https://apis.map.qq.com/ws/distance/v1/?mode=driving&from=39.983171,116.308479&to=39.996060,116.353455;39.949227,116.394310&key=OB4BZ-D4W3U-B7VVO-4PJWW-6TKDJ-WPB77
//        String json = null;
//        json = HttpUtils.requestUseGet(url, null);
//        System.out.println(json);
//    }

    public static void main(String[] args) {
        int n = 9;
        n = n >>> 1;
        System.out.println(n);
        n = 8;
        n |= n >>> 1;
        n = 1 << 2;
        System.out.println(n);
        System.out.println(0xff);
        System.out.println(0xff >>> 7);
        System.out.println((((byte) 0xff) >>> 7));
        System.out.println((byte) (((byte) 0xff) >>> 7));
        System.out.println(-8 >> 1);
        System.out.println(-8 >>> 1);
        System.out.println(tableSizeFor(-2018));
    }

    static final int MAXIMUM_CAPACITY = 1 << 30;

    static final int tableSizeFor(int cap) {
        int n = cap - 1;
        n |= n >>> 1;
        n |= n >>> 2;
        n |= n >>> 4;
        n |= n >>> 8;
        n |= n >>> 16;
        return (n < 0) ? 1 : (n >= MAXIMUM_CAPACITY) ? MAXIMUM_CAPACITY : n + 1;
    }

    private static double requestWithDistance(double distance) {
        double price =0;
        if (distance > 0 && distance <= 1000) {
            return 0;
        }
        if (distance > 7000 && distance <= 10000) {
            BigDecimal divide = BigDecimal.valueOf(distance - 7000).divide(BigDecimal.valueOf(1000), 0, BigDecimal.ROUND_UP);
            price += divide.multiply(BigDecimal.valueOf(5)).doubleValue();
            distance = 7000;
        }
        if (distance > 5000 && distance <= 7000) {
            BigDecimal divide = BigDecimal.valueOf(distance - 5000).divide(BigDecimal.valueOf(1000), 0, BigDecimal.ROUND_UP);
            price += divide.multiply(BigDecimal.valueOf(3)).doubleValue();
            distance = 5000;
        }
        if (distance > 3000 && distance <= 5000) {
            BigDecimal divide = BigDecimal.valueOf(distance - 3000).divide(BigDecimal.valueOf(1000), 0, BigDecimal.ROUND_UP);
            price += divide.multiply(BigDecimal.valueOf(2)).doubleValue();
            distance = 3000;
        }
        if (distance > 1000 && distance <= 3000) {
            BigDecimal divide = BigDecimal.valueOf(distance - 1000).divide(BigDecimal.valueOf(1000), 0, BigDecimal.ROUND_UP);
            price += divide.doubleValue();
        }
        return price;
    }

    private static BigDecimal requestBaseDeliverFee(String mtRange) {
        switch (mtRange) {
            case "1":
                return BigDecimal.valueOf(6.5);
            case "2":
                return BigDecimal.valueOf(6.0);
            case "3":
                return BigDecimal.valueOf(5.8);
            case "4":
                return BigDecimal.valueOf(5.5);
            case "5":
                return BigDecimal.valueOf(5.0);
            case "6":
                return BigDecimal.valueOf(4.7);
            default:
                return BigDecimal.valueOf(6.5);
        }
    }

}
