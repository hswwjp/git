package com.jie.helloservice.queue;

public class CircularQueue {
    // 数组：items，数组大小：n
    private String[] items;
    private int n = 0;
    // head 表示队头下标，tail 表示队尾下标
    private int head = 0;
    private int tail = 0;

    // 申请一个大小为 capacity 的数组
    public CircularQueue(int capacity) {
        items = new String[capacity];
        n = capacity;
    }

    // 假设有循环队列，下标如下 0, 1, 2, 3, 4, 5, 6, 7, 0, 1, 2 环形结构

    // 入队
    public boolean enqueue(String item) {
        // 队列满了
        if ((tail + 1) % n == head)
            return false;
        items[tail] = item;
        tail = (tail + 1) % n; // 当指针到达7这个下标时，那么需要将指针重新指向到0下标
        return true;
    }

    // 出队
    public String dequeue() {
        // 如果 head == tail 表示队列为空
        if (head == tail)
            return null;
        String ret = items[head];
        head = (head + 1) % n;
        return ret;
    }
}
