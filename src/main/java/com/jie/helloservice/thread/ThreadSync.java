package com.jie.helloservice.thread;

/**
 * 史上最难的 java 面试题
 */
public class ThreadSync implements Runnable {

    int b = 100;

    synchronized void m1() throws InterruptedException {
        b = 1000;
        //不释放对象锁
        Thread.sleep(500);//6
        System.out.println("b = " + b);
    }

    //直接作用于实例方法:相当于对当前实例加锁，进入同步代码前要获得当前实例的锁
    synchronized void m2() throws InterruptedException {
        //不释放对象锁
        Thread.sleep(250);//5
        b = 2000;
    }

    public static void main(String[] args) throws InterruptedException {
        ThreadSync ts = new ThreadSync();
        Thread t = new Thread(ts);//1
        t.start();//2

        ts.m2();//3
        System.out.println("main thread b = " + ts.b);//4
    }

    @Override
    public void run() {
        try {
            m1();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

