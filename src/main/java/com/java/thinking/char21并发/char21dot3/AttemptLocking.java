package com.java.thinking.char21并发.char21dot3;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 21.3.2 解决共享资源竞争
 * 使用显式的Lock对象
 */
public class AttemptLocking {

    private ReentrantLock lock = new ReentrantLock();

    /**
     * ReentrantLock允许你尝试着获取但最终未获取锁，这样如果其他人已经获取了这个锁。
     * 那你就可以决定离开去执行其他一些事情，而不是等待直至这个锁被释放，就像在untimed()方法中所看到的。
     */
    public void untimed() {
        boolean captured = lock.tryLock();
        try {
            System.out.println("tryLock(): " + captured);
        } finally {
            if (captured) {
                lock.unlock();
            }
        }
    }

    /**
     * 尝试去获取锁，该尝试可以在2秒之后失败
     */
    public void timed() {
        boolean captured = false;
        try {
            captured = lock.tryLock(2, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        try {
            System.out.println("tryLock(2, TimeUnit.SECONDS): " + captured);
        } finally {
            if (captured) {
                lock.unlock();
            }
        }
    }

    public static void main(String[] args) {
        final AttemptLocking al = new AttemptLocking();
        al.untimed(); // True -- lock is available
        al.timed();   // True -- lock is available

        // 作为匿名内部类而创建了一个单独的Thread
        // 使得untimed()和timed()方法对某些事物将产生竞争
        new Thread() {
            {
                setDaemon(true);
            }

            @Override
            public void run() {
                al.lock.lock();
                System.out.println("acquired");
            }
        }.start();
        Thread.yield(); // Give the 2nd task a chance
        al.untimed(); // False -- lock grabbed by task
        al.timed();   // False -- lock grabbed by task
    }
}
