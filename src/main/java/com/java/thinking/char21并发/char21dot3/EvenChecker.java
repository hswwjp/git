package com.java.thinking.char21并发.char21dot3;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 21.3.1 不正确地访问资源
 */
abstract class IntGenerator {
    private volatile boolean canceled = false;
    public abstract int next();
    public void cancel() {
        canceled = true;
    }

    public boolean isCanceled() {
        return canceled;
    }
}

public class EvenChecker implements Runnable {
    private IntGenerator generator;
    private final int id;

    public EvenChecker(IntGenerator g, int id) {
        this.generator = g;
        this.id = id;
    }

    @Override
    public void run() {
        while (!generator.isCanceled()) {
            int val = generator.next();
            if (val % 2 != 0) {
                System.out.println(val + " not even!");
                generator.cancel(); // Cancel all EvenCheckers
            }
        }
    }

    public static void test(IntGenerator gp, int count) {
        System.out.println("Press Control-C to exit");
        ExecutorService exec = Executors.newCachedThreadPool();
        for (int i = 0; i < count; i++) {
            exec.execute(new EvenChecker(gp, i));
        }
        exec.shutdown();
    }

    // Default value for count
    public static void test(IntGenerator gp) {
        test(gp, 10);
    }
}

class EvenGenerator extends IntGenerator {
    private int currentEvenValue = 0;

    /**
     * 一个任务有可能在另一个任务执行第一个对currentEvenValue的递增操作之后，但是没有执行第二个操作之前，调用next()方法。
     * 这将使这个值处于“不恰当”的状态。
     * @return
     */
    @Override
    public int next() {
        // 递增不是原子性的操作。如果不保护任务，即使单一的递增也不是安全的。
        ++currentEvenValue;// Danger point here;
//        Thread.yield();
        ++currentEvenValue;
        return currentEvenValue;
    }

    public static void main(String[] args) {
        EvenChecker.test(new EvenGenerator());
    }
}


/**
 * 同步控制EvenGenerator
 * 通过给next()方法加入synchronized关键字，可以防止不希望的线程访问
 */
class SynchronizedEvenGenerator extends IntGenerator {
    private int currentEvenValue = 0;


    /**
     * 如果在你的类中有超过一个方法在处理临界数据，那么你必须同步所有相关的方法。
     * 如果只同步一个方法，那么其他方法就会随意地忽略这个对象锁，并可以在无任何惩罚的情况下被调用。
     * 每个访问临界共享资源的方法都必须被同步，否则它们就不会正确地工作
     * @return
     */
    @Override
    public synchronized int next() {
        // 递增不是原子性的操作。如果不保护任务，即使单一的递增也不是安全的。
        ++currentEvenValue;// Danger point here;
        Thread.yield();
        ++currentEvenValue;
        return currentEvenValue;
    }

    public static void main(String[] args) {
        EvenChecker.test(new SynchronizedEvenGenerator());
    }
}

/**
 * 使用显示的Lock对象
 */
class MutexEvenGenerator extends IntGenerator {
    private int currentEvenValue = 0;
    private Lock lock = new ReentrantLock(); //重入锁

    /**
     * 如果在使用synchronized关键字时，某些事务失败了，那么就会抛出一个异常。
     * 但是没办法去做任何清理工作，以维护系统使其处于良好状态。
     * 有了显示的Lock对象，就可以使用finally子句将系统维护在正确的状态了。
     * @return
     */
    @Override
    public int next() {
        lock.lock();
        try {
            ++currentEvenValue;
            Thread.yield();
            ++currentEvenValue;
            //return语句必须在try子句中出现，以确保unlock()不会过早发生，从而将数据暴露给了第二个任务。
            return currentEvenValue;
        } finally {
            //必须在finally子句中带有unlock()方法
            lock.unlock();
        }
    }

    public static void main(String[] args) {
        EvenChecker.test(new MutexEvenGenerator());
    }
}