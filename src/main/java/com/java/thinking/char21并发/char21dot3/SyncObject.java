package com.java.thinking.char21并发.char21dot3;

/**
 * 21.3.6 在其他对象上同步
 */
class DualSynch {
    private Object syncObject = new Object();

    /**
     * f()方法在 this 同步
     */
    public synchronized void f() {
        for (int i = 0; i < 5; i++) {
            System.out.println("f()");
            Thread.yield();
        }
    }

    /**
     *  g()方法有一个在syncObject上同步的 synchronized 块
     */
    public void g() {
        synchronized (syncObject) {
            for (int i = 0; i < 5; i++) {
                System.out.println("g()");
                Thread.yield();
            }
        }
    }
}

/**
 * f()和g()是互相独立的
 * main()线程是被用来调用 g()的
 * 这两个方式在同时运行，因此任何一个方法都没有因为对另一个方法的同步而被阻塞
 */
public class SyncObject {
    public static void main(String[] args) {
        final DualSynch ds = new DualSynch();
        new Thread() {
            @Override
            public void run() {
                ds.f();
            }
        }.start();
        ds.g();
    }
}
