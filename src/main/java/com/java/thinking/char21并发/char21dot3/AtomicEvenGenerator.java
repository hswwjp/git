package com.java.thinking.char21并发.char21dot3;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 21.3.4 原子类
 */
public class AtomicEvenGenerator extends IntGenerator {
    private AtomicInteger currentEvenValue = new AtomicInteger(0);

    public int next() {
        return currentEvenValue.addAndGet(2);
    }

    public static void main(String[] args) {
        EvenChecker.test(new AtomicEvenGenerator());
    }
}
