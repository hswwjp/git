package com.java.thinking.char21并发.char21dot3;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 21.3.3 原子性和易变性
 */
public class AtomicityTest implements Runnable {
    private int i = 0;

    public int getValue() {
        return i;
    }

    private synchronized void evenIncrement() {
        i++;
        i++;
    }

    @Override
    public void run() {
        while (true) {
            evenIncrement();
        }
    }

    /**
     * 该程序找到奇数值并终止。尽管 return i 确实是原子性操作，但是缺少同步使得其数值可以在处于不稳定的中间状态时被读取。
     * 除此之外，由于 i 也不是 volatile 的，因此还存在可视性问题。
     * getValue()和evenIncrement()必须是 synchronized 的。
     * @param args
     */
    public static void main(String[] args) {
        ExecutorService exec = Executors.newCachedThreadPool();
        AtomicityTest at = new AtomicityTest();
        exec.execute(at);
        while (true) {
            int val = at.getValue();
            if (val % 2 != 0) {
                System.out.println(val);
                System.exit(0);
            }
        }
    }
}
