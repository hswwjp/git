package com.java.thinking.char21并发.char21dot3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 21.3.5 临界区
 */
class Pair { // 非线程安全
    private int x, y;

    public Pair(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Pair() {
        this(0, 0);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
    public void incrementX() {
        x++;
    }

    public void incrementY() {
        y++;
    }

    @Override
    public String toString() {
        return "Pair{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    public class PairValuesNotEqualException extends RuntimeException {
        public PairValuesNotEqualException() {
            super("Pair values not equal: " + Pair.this);
        }
    }

    // Arbitrary invariant -- both variables must be equal;
    public void checkState() {
        if (x != y) {
            throw new PairValuesNotEqualException();
        }
    }
}

/**
 * PairManager类的结构，它的一些功能在基类中实现，并且其一个或多个抽象方法在派生类中定义，这种结构在设计模式中称为模板方法。
 */
// Protect a Pair inside a thread-safe class
abstract class PairManager {
    AtomicInteger checkCounter = new AtomicInteger(0);
    protected Pair p = new Pair();
    private List<Pair> storage = Collections.synchronizedList(new ArrayList<Pair>());

    public synchronized Pair getPair() {
        // Make a copy to keep the original safe
        return new Pair(p.getX(), p.getY());
    }

    /**
     * 将一个 Pair 对象添加到了synchronizedList中，所以这个操作是线程安全的
     * @param p
     */
    // Assume this is a time consuming operation
    protected void store(Pair p) {
        storage.add(p);
        try {
            TimeUnit.MILLISECONDS.sleep(50);
        } catch (InterruptedException e) {

        }
    }
    public abstract void increment();
}

// Synchronize the entire method
class PairManager1 extends PairManager {
    /**
     * 整个increment()方法被同步控制
     * synchronized关键字不属于方法特征签名的组成部分，所以可以在覆盖方法的时候加上去
     */
    @Override
    public synchronized void increment() {
        p.incrementX();
        p.incrementY();
        store(getPair());
    }
}

// Use a critical section
class pairManager2 extends PairManager {

    /**
     * 通过使用同步控制块，而不是对整个方法进行同步控制，可以使多个任务访问对象的时间性能得到显著提高。
     */
    @Override
    public void increment() {
        Pair temp;
        // 同步控制块；在进入此段代码前，必须得到 this 对象的锁。
        // 如果其他线程已经得到这个锁，那么就得等到锁被释放以后，才能进入临界区。
        synchronized (this) {
            p.incrementX();
            p.incrementY();
            temp = getPair();
        }
        //store()线程安全，不必进行防护，可以放置在synchronized语句块的外部
        store(temp);
    }
}

class PairManipulator implements Runnable {
    private PairManager pm;

    public PairManipulator(PairManager pm) {
        this.pm = pm;
    }

    @Override
    public void run() {
        while (true)
            pm.increment();
    }

    @Override
    public String toString() {
        return "Pair: " + pm.getPair() + " checkCounter = " + pm.checkCounter.get();
    }
}

class PairChecker implements Runnable {
    private PairManager pm;

    public PairChecker(PairManager pm) {
        this.pm = pm;
    }

    @Override
    public void run() {
        while (true) {
            pm.checkCounter.incrementAndGet();
            pm.getPair().checkState();
        }
    }
}

/**
 * 对于 PairChecker 的检查频率，PairManager1.increment()不允许有pairManager2.increment()那样多
 * 后者采用同步控制块进行同步，所以对象不加锁的时间更长。
 * 这也是宁愿用同步控制块而不是对整个方法进行同步控制的典型原因：使得其他线程能更多地访问（在安全的情况下尽可能多）
 */
public class CriticalSection {
    // Test the two different approaches;
    static void testApproaches(PairManager pman1, PairManager pman2) {
        ExecutorService exec = Executors.newCachedThreadPool();
        PairManipulator pm1 = new PairManipulator(pman1),
                pm2 = new PairManipulator(pman2);
        PairChecker pcheck1 = new PairChecker(pman1),
                pcheck2 = new PairChecker(pman2);
        exec.execute(pm1);
        exec.execute(pm2);
        exec.execute(pcheck1);
        exec.execute(pcheck2);
        try {
            TimeUnit.MILLISECONDS.sleep(500);
        } catch (InterruptedException e) {
            System.out.println("Sleep interrupted");
        }
        System.out.println("pm1: " + pm1 + "\npm2: " + pm2);
        System.exit(0);
    }

    public static void main(String[] args) {
        PairManager pman1 = new PairManager1(),
                pman2 = new pairManager2();
        testApproaches(pman1, pman2);
    }
}
