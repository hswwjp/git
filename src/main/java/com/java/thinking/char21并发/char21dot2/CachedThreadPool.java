package com.java.thinking.char21并发.char21dot2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 21.2.3 使用Executor
 * Executors.newCachedThreadPool() 将为每个任务都创建一个线程
 * CachedThreadPool 在程序执行过程中通常会创建与所需数量相同的线程，然后在它回收旧线程时停止创建新线程，因此它是合理的Executor的首选
 */
public class CachedThreadPool {
    public static void main(String[] args) {
        ExecutorService exec = Executors.newCachedThreadPool();
        for (int i = 0; i < 5; i++) {
            exec.execute(new LiftOff());
        }
        exec.shutdown();
    }
}

/**
 * Executors.newFixedThreadPool(5) 使用了有限的线程集来执行所提交的任务
 * 可以一次性预先执行代价高昂的线程分配
 */
class FixedThreadPool {
    public static void main(String[] args) {
        // constructor argument is number of threads
        ExecutorService exec = Executors.newFixedThreadPool(5);
        for (int i = 0; i < 5; i++) {
            exec.execute(new LiftOff());
        }
        exec.shutdown();
    }
}

/**
 * SingleThreadExecutor就像是线程数量是1的FixedThreadPool。
 * 如果向SingleThreadExecutor提交了多个任务，那么这些任务将排队，每个任务都会在下一个任务开始之前运行结束，所有的任务将使用相同的线程。
 */
class SingleThreadExecutor {
    public static void main(String[] args) {
        ExecutorService exec = Executors.newSingleThreadScheduledExecutor();
        for (int i = 0; i < 5; i++) {
            exec.execute(new LiftOff());
        }
        exec.shutdown();
    }
}