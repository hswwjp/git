package com.java.thinking.char21并发.char21dot2;

import java.util.concurrent.TimeUnit;

/**
 * 21.2.8 后台线程
 */
public class DaemonFromFactory implements Runnable {
    @Override
    public void run() {
        try {
            while (true) {
                TimeUnit.MILLISECONDS.sleep(100);
                System.out.println(Thread.currentThread() + " " + this);
            }
        } catch (InterruptedException e) {
            System.out.println("Interrupted");
        }
    }

    public static void main(String[] args) throws InterruptedException {
//        ExecutorService exec = Executors.newCachedThreadPool(new DaemonFromFactory());
//        for (int i = 0; i < 10; i++) {
//            exec.execute(new DaemonFromFactory());
//        }
        System.out.println("All daemons started");
        TimeUnit.MILLISECONDS.sleep(500);
    }
}
