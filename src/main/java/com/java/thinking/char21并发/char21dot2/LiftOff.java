package com.java.thinking.char21并发.char21dot2;

/**
 * 21.2.1 定义任务
 */
public class LiftOff implements Runnable {
    protected int countDown = 10;
    private static int taskCount = 0;
    private final int id = taskCount++;
    public LiftOff() {}

    public LiftOff(int countDown) {
        this.countDown = countDown;
    }

    public String status() {
        return "#" + id + "(" + (countDown > 0 ? countDown : "Liftoff!") + "),";
    }

    @Override
    public void run() {
        while (countDown-- > 0) {
            System.out.print(status());
            Thread.yield();
        }
    }
}

class MainThread {
    public static void main(String[] args) {
        LiftOff launch = new LiftOff();
        launch.run();
    }
}

/**
 * 21.2.2 Thread类
 */
class BasicThreads {
    public static void main(String[] args) {
        Thread t = new Thread(new LiftOff());
        t.start();
        System.out.println("Waiting for LiftOff");
    }
}

/**
 * 创建多个线程
 */
class MoreBasicThreads {
    public static void main(String[] args) {
        for (int i = 0; i < 5; i++)
            new Thread(new LiftOff()).start();
        System.out.println("Waiting for LiftOff");
    }
}



class Practice1 {
    public Practice1() {
        System.out.println("start Practice1");
    }

    public static void main(String[] args) {
        Practice1Interface practice1Interface = new Practice1Interface();
        practice1Interface.run();
        System.out.println("end Practice1");
    }
}

class Practice1Interface implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 3; i++) {
            System.out.println("Practice1Interface running");
            Thread.yield();
        }
    }
}
