package com.java.thinking.char21并发.char21dot2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 21.2.14 捕获异常
 * 下面的任务总是会抛出一个异常，该异常会传播到其run()方法的外部
 */
public class ExceptionThread implements Runnable {
    @Override
    public void run() {
        throw new RuntimeException();
    }

    public static void main(String[] args) {
        ExecutorService exec = Executors.newCachedThreadPool();
        exec.execute(new ExceptionThread());
    }
}

/**
 * 将main的主体放到try-catch语句块中是没有作用的
 */
class NaiveExceptionHandling {
    public static void main(String[] args) {
        try {
            ExecutorService exec = Executors.newCachedThreadPool();
            exec.execute(new ExceptionThread());
        } catch (RuntimeException e) {
            System.out.println("Exception has been handled!");
        }
    }
}


