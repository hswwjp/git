package com.java.thinking.char21并发.char21dot2;

/**
 * 21.2.11 加入一个线程
 */
public class Sleeper extends Thread {
    private int duration;

    public Sleeper(String name, int sleepTime) {
        super(name);
        duration = sleepTime;
        start();
    }

    /**
     * sleep()方法有可能在指定的时间期满时返回，但也可能被中断。
     * 在catch子句中，将根据isInterrupted()的返回值报告这个中断。
     * 当另一个线程在该线程上调用interrupt()时，将给该线程设定一个标志，表明该线程已经被中断。
     * 然而，异常被捕获时将清理这个标志，所以在catch子句中，在异常被捕获的时候这个标志总是为假
     */
    @Override
    public void run() {
        try {
            sleep(duration);
        } catch (InterruptedException e) {
            System.out.println(getName() + " was interrupted. " + "isInterrupted(): " + isInterrupted());
            return;
        }
        System.out.println(getName() + " has awakened");
    }
}

class Joiner extends Thread {
    private Sleeper sleeper;

    public Joiner(String name, Sleeper sleeper) {
        super(name);
        this.sleeper = sleeper;
        start();
    }

    @Override
    public void run() {
        try {
            sleeper.join();
        } catch (InterruptedException e) {
            System.out.println("Interrupted");
        }
        System.out.println(getName() + " join completed");
    }
}

/**
 * Dopey和Doc调用Sleepy和Grumpy的join()方法，分别进入等待两个Sleeper醒来。
 * 这是main()方法走到grumpy.interrupt()，抛出了InterruptedException异常然后被捕获，然后return，Doc线程顺利走完。
 * 然后就是Sleepy线程正常醒来，然后Dopey线程走完
 */
class Joining {
    public static void main(String[] args) {
        Sleeper sleeper = new Sleeper("Sleepy", 1500),
                grumpy = new Sleeper("Grumpy", 1500);
        Joiner dopey = new Joiner("Dopey", sleeper),
               doc = new Joiner("Doc", grumpy);
        grumpy.interrupt();
    }
}