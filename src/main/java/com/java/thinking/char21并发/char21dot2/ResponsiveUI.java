package com.java.thinking.char21并发.char21dot2;

/**
 * 21.2.12 创建有响应的用户界面
 */
class UnresponsiveUI {
    private volatile double d = 1;
    public UnresponsiveUI() throws Exception {
        while (d > 0) {
            d = d + (Math.PI + Math.E) / d;
        }
        // 程序不可能到达读取控制台输入的那一行
        System.in.read();
    }
}

/**
 * 要想让程序有响应，就得把计算程序放在run()方法中，这样它就能让出处理器给别的程序。
 * 当你按下“回车”键的时候，可以看到计算确实在作为后台程序运行，同时还在等待用户输入。
 */
public class ResponsiveUI extends Thread {
    private static volatile double d = 1;
    public ResponsiveUI() {
        setDaemon(true);
        start();
    }

    @Override
    public void run() {
        while (true) {
            d = d + (Math.PI + Math.E) / d;
        }
    }

    public static void main(String[] args) throws Exception {
//        new UnresponsiveUI(); //Must kill this process
        new ResponsiveUI();
        System.in.read();
        System.out.println(d);
    }
}
