package com.java.thinking.char21并发.char21dot2;

import java.util.concurrent.TimeUnit;

/**
 * 21.2.8 后台线程
 * 后台进程在不执行finally子句的情况下就会终止其run()方法
 * 当最后一个非后台线程终止时，后台线程会“突然”终止。
 * 非后台的Executor通常是一种更好的方式，因为Executor控制的所有任务可以同时被关闭
 */
public class Adaemon implements Runnable {
    @Override
    public void run() {
        try {
            System.out.println("Starting Adaemon");
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            System.out.println("Exiting via InterruptedException");
        } finally {
            System.out.println("This should always run?");
        }
    }
}

class DaemonDontRunFinally {
    public static void main(String[] args) {
        Thread t = new Thread(new Adaemon());
        t.setDaemon(true);
        t.start();
    }
}