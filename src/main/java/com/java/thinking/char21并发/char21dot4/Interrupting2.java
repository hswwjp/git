package com.java.thinking.char21并发.char21dot4;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * BlockedMutex类有一个构造器，它要获取所创建对象上自身的 Lock，并且从不释放这个锁。
 */
class BlockedMutex {
    private Lock lock = new ReentrantLock();

    public BlockedMutex() {
        // Acquire it right away, to demonstrate interruption of a task blocked on a ReentrantLock:
        lock.lock();
    }

    /**
     * 基于构造器中获取锁并且从不释放
     * 如果试图从第二个任务中调用 f()（不同于创建这个BlockedMutex 的任务），那么将会总是因 Mutex 不可获得而被阻塞。
     */
    public void f() {
        try {
            // This will never be available to a second task
            lock.lockInterruptibly();
            System.out.println("lock acquired in f()");
        } catch (InterruptedException e) {
            System.out.println("Interrupted from lock acquisition in f()");
        }
    }
}

class Blocked2 implements Runnable {
    BlockedMutex blocked = new BlockedMutex();

    /**
     * run()方法总是在调用blocked.f()的地方停止。
     */
    @Override
    public void run() {
        System.out.println("等待BlockedMutex中的f()方法");
        blocked.f();
        System.out.println("Broken out of blocked call");
    }
}

public class Interrupting2 {
    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread(new Blocked2());
        t.start();
        TimeUnit.SECONDS.sleep(1);
        System.out.println("Issuing t.interrupt()");
        t.interrupt();
    }
}