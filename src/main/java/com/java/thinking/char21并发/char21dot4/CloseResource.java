package com.java.thinking.char21并发.char21dot4;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * 21.4.3 中断
 * 不能中断正在试图获取 synchronized锁或者试图执行 I/O操作的线程。
 * 特别是在创建执行 I/O的任务时，因为这意味着 I/O具有锁住你的多线程程序的潜在可能，这有点令人烦恼
 */

/**
 * 略显笨拙但是有时确实行之有效的解决方案，即关闭任务在其上发生阻塞的底层资源
 */
public class CloseResource {
    /**
     * 在 shutdown()被调用之后以及在两个输入流上调用 close()之前的延迟强调的是一旦底层资源被关闭，任务将解除阻塞。
     * 请注意，有一点很有趣，interrupt()看起来发生在关闭 Socket而不是关闭 System.in的时刻
     * @param args
     * @throws IOException
     * @throws InterruptedException
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        ExecutorService exec = Executors.newCachedThreadPool();
        ServerSocket server = new ServerSocket(8080);
        InputStream socketInput = new Socket("localhost", 8080).getInputStream();
        exec.execute(new IOBlocked(socketInput));
        exec.execute(new IOBlocked(System.in));
        TimeUnit.MILLISECONDS.sleep(100);
        System.out.println("关闭所有线程");
        exec.shutdown();
        TimeUnit.SECONDS.sleep(1);
        System.out.println("正在关闭 " + socketInput.getClass().getName());
        socketInput.close(); // 释放阻塞的线程
        TimeUnit.SECONDS.sleep(1);
        System.out.println("正在关闭 " + System.in.getClass().getName());
        System.in.close(); // 释放阻塞的线程
    }
}