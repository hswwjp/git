package com.java.thinking.char21并发.char21dot4;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * 21.4.3 中断
 */

/**
 * 可中断的阻塞示例
 * 从输出可以看到，能够中断对 sleep()的调用（或者任何要求抛出 InterruptedException的调用）
 */
class SleepBlocked implements Runnable {
    @Override
    public void run() {
        try {
            TimeUnit.SECONDS.sleep(100);
        } catch (InterruptedException e) {
            System.out.println("中断异常");
        }
        System.out.println("退出 SleepBlocked.run()");
    }
}

/**
 * 不可中断的阻塞示例
 * I/O 和在 synchronized块上的等待是不可中断的
 */
class IOBlocked implements Runnable {
    private InputStream in;

    public IOBlocked(InputStream in) {
        this.in = in;
    }

    @Override
    public void run() {
        System.out.println("等待输入:");
        try {
            in.read();
        } catch (IOException e) {
            if (Thread.currentThread().isInterrupted()) {
                System.out.println("Interrupted from blocked I/O");
            } else {
                throw new RuntimeException(e);
            }
        }
    }
}

/**
 * 不可中断的阻塞示例
 */
class SynchronizedBlocked implements Runnable {
    /**
     *  f() 永远都不返回，因此这个锁永远不会释放
     */
    public synchronized void f() {
        while (true) {
            Thread.yield();
        }
    }

    /**
     *
     */
    public SynchronizedBlocked() {
        new Thread() {
            @Override
            public void run() {
                f();
            }
        }.start();
    }

    /**
     *  试图调用 f()，并阻塞以等待这个锁被释放
     */
    @Override
    public void run() {
        System.out.println("尝试调用 f()");
        f();
        System.out.println("退出 SynchronizedBlocked.run()");
    }
}

/**
 * 如果在 Executor上调用shutdownNow()，那么它将发送一个interrupt()调用给它启动的所有线程。
 * 这么做是有意义的，因为当你完成工程中的某个部分或者整个程序时，通常会希望同时关闭某个特定 Executor的所有任务。
 *
 * 通过调用 submit()而不是 executor()来启动任务，就可以持有该任务的上下文。
 * submit()将返回一个泛型 Future<?>，其中有一个未修饰的参数
 * 持有这种 Future的关键在于你可以在其上调用 cancel()，并因此可以使用它来中断某个特定任务。
 * 如果将 true 传递给 cancel()，那么它就会拥有在该线程上调用 interrupt()以停止这个线程的权限。
 * 因此, cancel()是一种中断由 Executor启动的单个线程的方式
 */
public class Interrupting {
    private static ExecutorService exec = Executors.newCachedThreadPool();
    static void test(Runnable r) throws InterruptedException {
        Future<?> f = exec.submit(r);
        TimeUnit.MILLISECONDS.sleep(100);
        System.out.println("打断 " + r.getClass().getName());
        f.cancel(true); // Interrupts if running
        System.out.println("打断发送到 " + r.getClass().getName());
    }

    public static void main(String[] args) throws InterruptedException {
        test(new SleepBlocked());
        test(new IOBlocked(System.in));
        test(new SynchronizedBlocked());
        TimeUnit.SECONDS.sleep(3);
        System.out.println("Aborting with System.exit(0)");
        System.exit(0); // ... since last 2 interrupts failed
    }
}