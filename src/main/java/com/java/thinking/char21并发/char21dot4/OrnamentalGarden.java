package com.java.thinking.char21并发.char21dot4;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * 21.4.1 装饰性花园
 */
class Count {
    private int count = 0;
    private Random rand = new Random(47);

    /**
     * 如果移除 synchronized声明，就会总人数与期望有差异，每个 Entrance统计的人数与 count中的值不同
     * 通过使用 temp和 yield()，增加了失败的可能性
     * @return
     */
    public synchronized int increment() {
        int temp = count;
        if (rand.nextBoolean()) // Yield half the time
            Thread.yield();
        return (count = ++temp);
    }

    public synchronized int value() {
        return count;
    }
}

class Entrance implements Runnable {
    private static Count count = new Count();
    private static List<Entrance> entrances = new ArrayList<Entrance>();
    private int number = 0;
    // Doesn't need synchronization to read
    private final int id;
    /**
     * Entrance.canceled是一个 volatile布尔标志，而它只会被读取和赋值（不会与其他域组合在一起被读取），所以不需要同步对其的访问，就可以安全的操作它
     * 如果对诸如此类的情况有任何疑虑，那么最好总是使用 synchronized
     */
    private static volatile boolean canceled = false;

    // Atomic operation on a volatile field
    public static void cancel() {
        canceled = true;
    }

    public Entrance(int id) {
        this.id = id;
        // Keep this task in a list. Also prevents garbage collection of dead tasks
        entrances.add(this);
    }

    @Override
    public void run() {
        while (!canceled) {
            synchronized (this) {
                ++number;
            }
            System.out.println(this + " Count: " + count.increment());
            try {
                TimeUnit.MILLISECONDS.sleep(100);
            } catch (InterruptedException e) {
                System.out.println("sleep interrupted");
            }
        }
        System.out.println("Stopping " + this);
    }

    public synchronized int getValue() {
        return number;
    }

    public String toString() {
        return "Entrance " + id + ": Number " + getValue();
    }

    public static int getTotalCount() {
        return count.value();
    }

    public static int sumEntrances() {
        int sum = 0;
        for (Entrance entrance : entrances) {
            sum += entrance.getValue();
        }
        return sum;
    }
}

public class OrnamentalGarden {
    public static void main(String[] args) throws InterruptedException {
        ExecutorService exec = Executors.newCachedThreadPool();
        for (int i = 0; i < 5; i++) {
            exec.execute(new Entrance(i));
        }
        // Run for a while, then stop and collect the data
        TimeUnit.SECONDS.sleep(3);
        Entrance.cancel();
        exec.shutdown();
        // awaitTermination() 方法等待每个任务结束，如果所有的任务在超时时间达到之前全部结束，则返回true，否则返回false
        if (!exec.awaitTermination(250, TimeUnit.MILLISECONDS)) {
            System.out.println("Some tasks were not terminated!");
        }
        // 获取总人数
        System.out.println("Count: " + Entrance.getTotalCount());
        // 统计5个 Entrance的 Number和
        System.out.println("Sum of Entrances Number: " + Entrance.sumEntrances());
    }
}
